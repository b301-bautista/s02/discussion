package com.zuitt.example;

public class ControlStructure {
    public static void main (String[] args) {
        // If statements
        int num1 = 10;
        if(num1 > 0){
            System.out.println("the number is positive");
        }
        else if(num1 < 0){
            System.out.println("The number is negative");
        }
        else{
            System.out.println("The number is zero (not positive nor negative)");
        }

        // Switch cases:

        int directionValue = 4;
        switch(directionValue){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");

        }
    }
}
